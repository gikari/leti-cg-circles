#ifndef SURFACESGLWIDGET_H
#define SURFACESGLWIDGET_H

#include "nicglwidget.h"
#include "../calculations/bsplinesurface.h"
#include <QObject>
#include <QVector>
#include <QVector3D>

class SurfacesGLWidget : public NicGLWidget
{
    Q_OBJECT
public:
    SurfacesGLWidget(QWidget* parent = nullptr);

    void plotSurface();

    const QVector<QVector<QVector3D>>& getPolyheron() const;
    QVector3D& getPolyhedronPointAt(int row, int column);

public slots:
    void updatePointX(qreal value, QPoint pointIndexes);
    void updatePointY(qreal value, QPoint pointIndexes);
    void updatePointZ(qreal value, QPoint pointIndexes);

    void setDegreeU(int degree);
    void setDegreeW(int degree);

    void toggleRealtimeUpdate(bool value);

    void rotateAroundOX(double degrees);
    void rotateAroundOY(double degrees);
    void rotateAroundOZ(double degrees);

protected:
    void paintGL() override;

private:
    void updateInRealtimeIfPermitted();

    BSplineSurface surface;
    bool isRealTimeUpdatePermitted;
    double currentRotationAroundOX;
    double currentRotationAroundOY;
    double currentRotationAroundOZ;
};

#endif // SURFACESGLWIDGET_H
