#ifndef NICGLWIDGET_H
#define NICGLWIDGET_H

#include <QOpenGLWidget>
#include <QObject>
#include <QWidget>
#include <QMap>
#include <QString>
#include <QVector3D>

class NicGLWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    NicGLWidget(QWidget* parent = nullptr);
    ~NicGLWidget();

protected:
    void initializeGL();
    void resizeGL(int width, int height);

    double deg2rad (double degrees);
    float deg2rad (float degrees);

    void plotCircle(double x, double y, double radius);
    void plotBullet(float x, float y, std::array<double, 3> color = {1, 1, 1});
    void plotBullet(double x, double y, std::array<double, 3> color = {1, 1, 1});

    void plotLightSourceImage(float x, float y, float z);
};

#endif // NICGLWIDGET_H
