#include "surfacesglwidget.h"
#include "nicglwidget.h"
#include "../calculations/bsplinesurface.h"

#include <QRandomGenerator>
#include <QVector>
#include <QVector3D>

SurfacesGLWidget::SurfacesGLWidget(QWidget* parent) : NicGLWidget(parent),
    surface{}, isRealTimeUpdatePermitted{false},
    currentRotationAroundOX{0},
    currentRotationAroundOY{0},
    currentRotationAroundOZ{0}
{
}

void SurfacesGLWidget::plotSurface()
{

    for (float u = 0.1F; u <= 1; u += 0.01F) {
        glBegin(GL_LINE_STRIP);
            for (float w = 0.1F; w <= 1; w += 0.05F) {
                QVector3D surfaceEval = surface.eval(u, w);
                glColor3f(1 - u, 1 - w, 0.5);
                glVertex3f(surfaceEval.x(), surfaceEval.y(), surfaceEval.z());
            }
        glEnd();
    }

    // Second time for a mesh
    for (float u = 0.1F; u <= 1; u += 0.01F) {
        glBegin(GL_LINE_STRIP);
            for (float w = 0.1F; w <= 1; w += 0.05F) {
                QVector3D surfaceEval = surface.eval(w, u);
                glColor3f(1 - u, 1 - w, 0.5);
                glVertex3f(surfaceEval.x(), surfaceEval.y(), surfaceEval.z());
            }
        glEnd();
    }

    QVector<QVector<QVector3D>> controlPoints = surface.getPolyheron();
    for (const QVector<QVector3D>& row : controlPoints) {
        for (const QVector3D& point : row) {
            plotBullet(point.x(), point.y());
        }
    }

}

const QVector<QVector<QVector3D> > &SurfacesGLWidget::getPolyheron() const
{
    return surface.getPolyheron();
}

QVector3D &SurfacesGLWidget::getPolyhedronPointAt(int row, int column)
{
    return surface.getPolyhedronPointAt(row, column);
}

void SurfacesGLWidget::updatePointX(qreal value, QPoint pointIndexes)
{
    surface.updatePointX(float(value), pointIndexes);
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::updatePointY(qreal value, QPoint pointIndexes)
{
    surface.updatePointY(float(value), pointIndexes);
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::updatePointZ(qreal value, QPoint pointIndexes)
{
    surface.updatePointZ(float(value), pointIndexes);
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::setDegreeU(int degree)
{
    surface.setDegreeU(degree);
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::setDegreeW(int degree)
{
    surface.setDegreeW(degree);
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::toggleRealtimeUpdate(bool value)
{
    isRealTimeUpdatePermitted = value;
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::rotateAroundOX(double degrees)
{
    double deltaDegrees = currentRotationAroundOX - degrees;
    if (std::abs(deltaDegrees) >= 0.01) {
        surface.rotateAroundOX(float(deltaDegrees));
        currentRotationAroundOX = degrees;
    }
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::rotateAroundOY(double degrees)
{
    double deltaDegrees = currentRotationAroundOY - degrees;
    if (std::abs(deltaDegrees) >= 0.01) {
        surface.rotateAroundOY(float(deltaDegrees));
        currentRotationAroundOY = degrees;
    }
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::rotateAroundOZ(double degrees)
{
    double deltaDegrees = currentRotationAroundOZ - degrees;
    if (std::abs(deltaDegrees) >= 0.01) {
        surface.rotateAroundOZ(float(deltaDegrees));
        currentRotationAroundOZ = degrees;
    }
    updateInRealtimeIfPermitted();
}

void SurfacesGLWidget::updateInRealtimeIfPermitted()
{
    if (isRealTimeUpdatePermitted) {
        update();
    }
}

void SurfacesGLWidget::paintGL()
{
    plotSurface();
}
