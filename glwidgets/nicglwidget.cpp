#include "nicglwidget.h"
#include <QDebug>
#include <cmath>
#include <QMap>
#include <QString>
#include <QOpenGLWidget>
#include <GL/freeglut.h>
#include <GL/glu.h>
#include <QMatrix4x4>
#include <cmath>

NicGLWidget::NicGLWidget(QWidget* parent)
    : QOpenGLWidget(parent)
{}

NicGLWidget::~NicGLWidget()
{}

void NicGLWidget::initializeGL() {
    glClearColor(0x0, 0x0, 0x0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void NicGLWidget::resizeGL(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, double(w) / h, 0.01, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);
}


double NicGLWidget::deg2rad (double degrees) {
    static const double pi_on_180 = M_PI / 180.0;
    return degrees * pi_on_180;
}

float NicGLWidget::deg2rad (float degrees) {
    return float(deg2rad(double(degrees)));
}

void NicGLWidget::plotCircle(double x0, double y0, double radius)
{
    const int pieceAmount = 36;
    const double twicePi = 2.0 * M_PI;
    const double delta_alfa = twicePi / pieceAmount;
    double c1 = cos(delta_alfa);
    double c2 = sin(delta_alfa);
    double xn = x0 - radius;
    double yn = y0;

    glBegin(GL_LINE_LOOP);
        glColor3f(1, 1, 1);
        for(int pieceNumber = 0; pieceNumber < pieceAmount; pieceNumber++) {
            double xnew = x0 + c1*(xn - x0) - c2*(yn - y0);
            double ynew = y0 + c1*(yn - y0) + c2*(xn - x0);
            glVertex2d(xnew, ynew);
            xn = xnew;
            yn = ynew;
        }
    glEnd();
}


void NicGLWidget::plotBullet(float x, float y, std::array<double, 3> color) {
    plotBullet(double(x), double(y), color);
}

void NicGLWidget::plotBullet(double x, double y, std::array<double, 3> color)
{
    const double radius = 0.02;
    const int pieceAmount = 36;
    const double twicePi = 2.0 * M_PI;
    const double delta_alfa = twicePi / pieceAmount;
    double c1 = cos(delta_alfa);
    double c2 = sin(delta_alfa);
    double xn = x - radius;
    double yn = y;

    glBegin(GL_POLYGON);
        glColor3d(color[0], color[1], color[2]);
        for(int pieceNumber = 0; pieceNumber < pieceAmount; pieceNumber++) {
            double xnew = x + c1*(xn - x) - c2*(yn - y);
            double ynew = y + c1*(yn - y) + c2*(xn - x);
            glVertex2d(xnew, ynew);
            xn = xnew;
            yn = ynew;
        }
        glEnd();
}

void NicGLWidget::plotLightSourceImage(float x, float y, float z)
{
    glColor3f(1, 1, 0);
    glBegin(GL_TRIANGLES);
        glNormal3f(-1.0f, 0, 0);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);
        glVertex3f(x-0.05f,y-0.05f,z+0.05f);
        glVertex3f(x-0.05f,y+0.05f,z+0.05f);

        glNormal3f(0, 0, -1.0f);
        glVertex3f(x+0.05f,y+0.05f,z-0.05f);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);
        glVertex3f(x-0.05f,y+0.05f,z-0.05f);

        glNormal3f(0, -1.0f, 0);
        glVertex3f(x+0.05f,y-0.05f,z+0.05f);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);
        glVertex3f(x+0.05f,y-0.05f,z-0.05f);

        glNormal3f(0, 0, -1.0f);
        glVertex3f(x+0.05f,y+0.05f,z-0.05f);
        glVertex3f(x+0.05f,y-0.05f,z-0.05f);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);

        glNormal3f(-1.0f, 0, 0);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);
        glVertex3f(x-0.05f,y+0.05f,z+0.05f);
        glVertex3f(x-0.05f,y+0.05f,z-0.05f);

        glNormal3f(0, -1.0f, 0);
        glVertex3f(x+0.05f,y-0.05f,z+0.05f);
        glVertex3f(x-0.05f,y-0.05f,z+0.05f);
        glVertex3f(x-0.05f,y-0.05f,z-0.05f);

        glNormal3f(0, 0, 1.0f);
        glVertex3f(x-0.05f,y+0.05f,z+0.05f);
        glVertex3f(x-0.05f,y-0.05f,z+0.05f);
        glVertex3f(x+0.05f,y-0.05f,z+0.05f);

        glNormal3f(1.0f, 0, 0);
        glVertex3f(x+0.05f,y+0.05f,z+0.05f);
        glVertex3f(x+0.05f,y-0.05f,z-0.05f);
        glVertex3f(x+0.05f,y+0.05f,z-0.05f);

        glNormal3f(1.0f, 0, 0);
        glVertex3f(x+0.05f,y-0.05f,z-0.05f);
        glVertex3f(x+0.05f,y+0.05f,z+0.05f);
        glVertex3f(x+0.05f,y-0.05f,z+0.05f);

        glNormal3f(0, 1.0f, 0);
        glVertex3f(x+0.05f,y+0.05f,z+0.05f);
        glVertex3f(x+0.05f,y+0.05f,z-0.05f);
        glVertex3f(x-0.05f,y+0.05f,z-0.05f);

        glNormal3f(0, 1.0f, 0);
        glVertex3f(x+0.05f,y+0.05f,z+0.05f);
        glVertex3f(x-0.05f,y+0.05f,z-0.05f);
        glVertex3f(x-0.05f,y+0.05f,z+0.05f);

        glNormal3f(0, 0, 1.0f);
        glVertex3f(x+0.05f,y+0.05f,z+0.05f);
        glVertex3f(x-0.05f,y+0.05f,z+0.05f);
        glVertex3f(x+0.05f,y-0.05f,z+0.05f);
    glEnd();
}


