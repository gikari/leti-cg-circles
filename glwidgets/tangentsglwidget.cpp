#include "tangentsglwidget.h"
#include <QOpenGLWidget>
#include <GL/freeglut.h>
#include <GL/glu.h>
#include <cmath>
#include <QDebug>

TangentsGLWidget::TangentsGLWidget(QWidget* parent) :
    NicGLWidget(parent),
      data{
          {
              "first-circle", {
                    {"x", 0.5},
                    {"y", 0.5},
                    {"r", 1},
                    {"number", -1},
              }
          },
          {
              "second-circle", {
                    {"x", -1},
                    {"y", -1},
                    {"r", 0.5},
                    {"number", -1},
              }
          },
          }
{
    data["first-circle"]["number"] = arith_date.create_new_circle_2d(data["first-circle"]["x"], data.value("first-circle").value("y"), data.value("first-circle").value("r"));
    data["second-circle"]["number"] = arith_date.create_new_circle_2d(data["second-circle"]["x"], data.value("second-circle").value("y"), data.value("second-circle").value("r"));
}

void TangentsGLWidget::plotCircles()
{
    double x1 = data.value("first-circle").value("x");
    double y1 = data.value("first-circle").value("y");
    double r1 = data.value("first-circle").value("r");
    double x2 = data.value("second-circle").value("x");
    double y2 = data.value("second-circle").value("y");
    double r2 = data.value("second-circle").value("r");

    plotCircle(x1, y1, 0.02);
    plotCircle(x2, y2, 0.02);
    plotCircle(x1, y1, r1);
    plotCircle(x2, y2, r2);
}

void TangentsGLWidget::setX1(double value)
{
    data["first-circle"]["x"] = value;
    arith_date.change_circle(data.value("first-circle").value("number"), data["first-circle"]["x"], data.value("first-circle").value("y"), data.value("first-circle").value("r"));
    update();
}

void TangentsGLWidget::setY1(double value)
{
    data["first-circle"]["y"] = value;
    arith_date.change_circle(data.value("first-circle").value("number"), data["first-circle"]["x"], data.value("first-circle").value("y"), data.value("first-circle").value("r"));
    update();
}

void TangentsGLWidget::setR1(double value)
{
    data["first-circle"]["r"] = value;
    arith_date.change_circle(data.value("first-circle").value("number"), data["first-circle"]["x"], data.value("first-circle").value("y"), data.value("first-circle").value("r"));
    update();
}

void TangentsGLWidget::setX2(double value)
{
    data["second-circle"]["x"] = value;
    arith_date.change_circle(data.value("second-circle").value("number"), data["second-circle"]["x"], data.value("second-circle").value("y"), data.value("second-circle").value("r"));
    update();
}

void TangentsGLWidget::setY2(double value)
{
    data["second-circle"]["y"] = value;
    arith_date.change_circle(data.value("second-circle").value("number"), data["second-circle"]["x"], data.value("second-circle").value("y"), data.value("second-circle").value("r"));
    update();
}

void TangentsGLWidget::setR2(double value)
{
    data["second-circle"]["r"] = value;
    arith_date.change_circle(data.value("second-circle").value("number"), data["second-circle"]["x"], data.value("second-circle").value("y"), data.value("second-circle").value("r"));
    update();
}



void TangentsGLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    plotCircles();

    bool flag = true; // Сделать на UI галочку, меняющая эту переменную

    if (flag)
    {
        plotTangent();

    }
    glBegin(GL_LINES);
    glVertex2d(data.value("first-circle").value("x"), data.value("first-circle").value("y"));
    glVertex2d(data.value("second-circle").value("x"), data.value("second-circle").value("y"));
    glEnd();

    arith_date.points_view_debug();
}

void TangentsGLWidget::plotTangent()
{

//    qDebug() << "Radisus : " << data.value("first-circle").value("number") << data.value("second-circle").value("number");

    int count_point_tangent = arith_date.tangent_circles_2d(data.value("first-circle").value("number"), data.value("second-circle").value("number"));

//    qDebug() << "POINT_1 :" << count_point_tangent;

    QMap<QString, QMap<QString, double>> buf_main, buf_out_ui;
    QMap<QString, QMap<QString, double>> buf0;
    QMap<QString, double> buf1;
    QMap<QString, double> buf2;
    for (int i = count_point_tangent; i > 0; i -= 2)
    {
        glBegin(GL_LINES);
        buf1 = arith_date.pop_element();
        buf2 = arith_date.pop_element();
        switch (i) {
        case 2:
            buf_out_ui.insert("out-first-tangent-1", buf1);
            buf_out_ui.insert("out-first-tangent-2", buf2);
            break;
        case 4:
            buf_out_ui.insert("out-second-tangent-1", buf1);
            buf_out_ui.insert("out-second-tangent-2", buf2);
            break;
        case 6:
            buf_out_ui.insert("inner-first-tangent-1", buf1);
            buf_out_ui.insert("inner-first-tangent-2", buf2);
            break;
        case 8:
            buf_out_ui.insert("inner-second-tangent-1", buf1);
            buf_out_ui.insert("inner-second-tangent-2", buf2);
            break;
        default:
            qDebug() << "Warning!!! System bug!";
            break;
        }
//        qDebug() << "POINT_1 : buf2.value(x) = " << buf2.value("x") << "; buf2.value(y) = " << buf2.value("y") << "; buf1.value(x) = " << buf1.value("x") << "; buf1.value(y) = " << buf1.value("y");
        buf_main = arith_date.segment_into_line(buf2.value("x"), buf2.value("y"), buf1.value("x"), buf1.value("y"), 10);
        glVertex2d(buf_main.value("first-point").value("x"), buf_main.value("first-point").value("y"));
        glVertex2d(buf_main.value("second-point").value("x"), buf_main.value("second-point").value("y"));
//        qDebug() << "Plotted tangent with X: " << buf_main.value("first-point").value("x") << "; Y: " << buf_main.value("first-point").value("y") << ";";
//        qDebug() << "Plotted tangent with X: " << buf_main.value("second-point").value("x") << "; Y: " << buf_main.value("second-point").value("y") << ";";
        glEnd();
    }

    emit out_first_tangent_x1_changed(buf_out_ui.value("out-first-tangent-1").value("x"));
    emit out_first_tangent_y1_changed(buf_out_ui.value("out-first-tangent-1").value("y"));
    emit out_first_tangent_x2_changed(buf_out_ui.value("out-first-tangent-2").value("x"));
    emit out_first_tangent_y2_changed(buf_out_ui.value("out-first-tangent-2").value("y"));
    emit out_second_tangent_x1_changed(buf_out_ui.value("out-second-tangent-1").value("x"));
    emit out_second_tangent_y1_changed(buf_out_ui.value("out-second-tangent-1").value("y"));
    emit out_second_tangent_x2_changed(buf_out_ui.value("out-second-tangent-2").value("x"));
    emit out_second_tangent_y2_changed(buf_out_ui.value("out-second-tangent-2").value("y"));
    emit inner_first_tangent_x1_changed(buf_out_ui.value("inner-first-tangent-1").value("x"));
    emit inner_first_tangent_y1_changed(buf_out_ui.value("inner-first-tangent-1").value("y"));
    emit inner_first_tangent_x2_changed(buf_out_ui.value("inner-first-tangent-2").value("x"));
    emit inner_first_tangent_y2_changed(buf_out_ui.value("inner-first-tangent-2").value("y"));
    emit inner_second_tangent_x1_changed(buf_out_ui.value("inner-second-tangent-1").value("x"));
    emit inner_second_tangent_y1_changed(buf_out_ui.value("inner-second-tangent-1").value("y"));
    emit inner_second_tangent_x2_changed(buf_out_ui.value("inner-second-tangent-2").value("x"));
    emit inner_second_tangent_y2_changed(buf_out_ui.value("inner-second-tangent-2").value("y"));

}

