#include "lightglwidget.h"
#include <cmath>
#include <QMatrix4x4>
#include <GL/glu.h>

LightGLWidget::LightGLWidget(QWidget* parent) :
    NicGLWidget(parent),
    cameraPosition{0, 0, 5},
    lightPosition{ 1.3F, 1.3F, 1.3F },
    cameraRotationAngleAroundOY{0},
    lightRotationAngleAroundOY{0},
    lightIntensity{1}
{

}

void LightGLWidget::setCameraPositionX(double value)
{
    cameraPosition.setX(float(value));
    update();
}

void LightGLWidget::setCameraPositionY(double value)
{
    cameraPosition.setY(float(value));
    update();
}

void LightGLWidget::setCameraPositionZ(double value)
{
    cameraPosition.setZ(float(value));
    update();
}

void LightGLWidget::setCameraRotationAngle(double angle)
{
    float angleInRad = deg2rad(float(angle));
    float delta = angleInRad - cameraRotationAngleAroundOY;

    using namespace std;
    QMatrix4x4 transformMatrix{
        cos(delta), 0, -sin(delta), 0,
             0, 1,       0, 0,
        sin(delta), 0,  cos(delta), 0,
             0, 0,       0, 1
    };

    cameraRotationAngleAroundOY = angleInRad;
    cameraPosition = transformMatrix.mapVector(cameraPosition);
    emit cameraPositionXUpdated(double(cameraPosition.x()));
    emit cameraPositionYUpdated(double(cameraPosition.y()));
    emit cameraPositionZUpdated(double(cameraPosition.z()));
    update();
}

void LightGLWidget::setLightPositionX(double value) {
    lightPosition.setX(float(value));
    update();
}

void LightGLWidget::setLightPositionY(double value) {
    lightPosition.setY(float(value));
    update();
}

void LightGLWidget::setLightPositionZ(double value) {
    lightPosition.setZ(float(value));
    update();
}

void LightGLWidget::setLightIntensity(double value)
{
    lightIntensity = float(value);
    update();
}

void LightGLWidget::setLightRotationAngle(double angle)
{
    float angleInRad = deg2rad(float(angle));
    float delta = angleInRad - lightRotationAngleAroundOY;

    using namespace std;
    QMatrix4x4 transformMatrix{
        cos(delta), 0, -sin(delta), 0,
             0, 1,       0, 0,
        sin(delta), 0,  cos(delta), 0,
             0, 0,       0, 1
    };

    lightRotationAngleAroundOY = angleInRad;
    lightPosition = transformMatrix.mapVector(lightPosition);
    emit lightPositionXUpdated(double(lightPosition.x()));
    emit lightPositionYUpdated(double(lightPosition.y()));
    emit lightPositionZUpdated(double(lightPosition.z()));
    update();
}

void LightGLWidget::initializeGL()
{
    NicGLWidget::initializeGL();

    GLfloat lp[4] = {
        lightPosition.x(),
        lightPosition.y(),
        lightPosition.z(),
        1.0
    };
    glLightfv(GL_LIGHT0, GL_POSITION, lp);


    GLfloat lac[4] = { 0, 0, 0, 1 };
    glLightfv(GL_LIGHT0, GL_AMBIENT, lac);

    GLfloat ldc[4] = { 1, 1, 1, 1 };
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ldc);

    GLfloat lsc[4] = { 1, 1, 1, 1 };
    glLightfv(GL_LIGHT0, GL_SPECULAR, lsc);
}

void LightGLWidget::paintGL()
{
    plotStdandardCube();

    plotLightSourceImage(lightPosition.x(), lightPosition.y(), lightPosition.z());

    // Update light
    GLfloat lp[4] = {
        lightPosition.x(),
        lightPosition.y(),
        lightPosition.z(),
        1.0
    };
    glLightfv(GL_LIGHT0, GL_POSITION, lp);

    // Light Intensity
    GLfloat ldc[4] = {lightIntensity, lightIntensity, lightIntensity, 1 };
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ldc);

    // Update rotation
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(double(cameraPosition.x()),
              double(cameraPosition.y()),
              double(cameraPosition.z()),
              0, 0, 0, 0, 1, 0);
}

void LightGLWidget::resizeGL(int width, int height)
{
    NicGLWidget::resizeGL(width, height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(double(cameraPosition.x()),
              double(cameraPosition.y()),
              double(cameraPosition.z()),
              0, 0, 0, 0, 1, 0);
}

void LightGLWidget::plotStdandardCube()
{
    glColor3d(1, 0.38, 0.57);

    glBegin(GL_TRIANGLES);
        glNormal3f(-1.0f, 0, 0);
        glVertex3f(-1.0f,-1.0f,-1.0f);
        glVertex3f(-1.0f,-1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glNormal3f(0, 0, -1.0f);
        glVertex3f(1.0f, 1.0f,-1.0f);
        glVertex3f(-1.0f,-1.0f,-1.0f);
        glVertex3f(-1.0f, 1.0f,-1.0f);

        glNormal3f(0, -1.0f, 0);
        glVertex3f(1.0f,-1.0f, 1.0f);
        glVertex3f(-1.0f,-1.0f,-1.0f);
        glVertex3f(1.0f,-1.0f,-1.0f);

        glNormal3f(0, 0, -1.0f);
        glVertex3f(1.0f, 1.0f,-1.0f);
        glVertex3f(1.0f,-1.0f,-1.0f);
        glVertex3f(-1.0f,-1.0f,-1.0f);

        glNormal3f(-1.0f, 0, 0);
        glVertex3f(-1.0f,-1.0f,-1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f,-1.0f);

        glNormal3f(0, -1.0f, 0);
        glVertex3f(1.0f,-1.0f, 1.0f);
        glVertex3f(-1.0f,-1.0f, 1.0f);
        glVertex3f(-1.0f,-1.0f,-1.0f);

        glNormal3f(0, 0, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f,-1.0f, 1.0f);
        glVertex3f(1.0f,-1.0f, 1.0f);

        glNormal3f(1.0f, 0, 0);
        glVertex3f(1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f,-1.0f,-1.0f);
        glVertex3f(1.0f, 1.0f,-1.0f);

        glNormal3f(1.0f, 0, 0);
        glVertex3f(1.0f,-1.0f,-1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f,-1.0f, 1.0f);

        glNormal3f(0, 1.0f, 0);
        glVertex3f(1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f, 1.0f,-1.0f);
        glVertex3f(-1.0f, 1.0f,-1.0f);

        glNormal3f(0, 1.0f, 0);
        glVertex3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f,-1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glNormal3f(0, 0, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f,-1.0f, 1.0);
    glEnd();

}
