#include "curvesglwidget.h"
#include <QDebug>
#include <GL/freeglut.h>
#include <GL/glu.h>
#include "../calculations/bspline.h"

CurvesGLWidget::CurvesGLWidget(QWidget *parent) :
    NicGLWidget (parent),
    bullets {
        {-1, 0, 0},
        {-2, 1, 0},
        {-1, 2, 0},
        {0, 1, 0},
        {1, 2, 0},
        {2, 1, 0},
        {1, 0, 0}
        },
    splineDegree{3},
    knots{
        0, 0, 0,
        0, 0.5F, 0.6F,
        0.7F, 1, 1,
        1, 1
        },
    savedSplines{}
{}

void CurvesGLWidget::setSplineDegree(int degree) {
    splineDegree = degree;
    resizeKnots();
    update();
}

void CurvesGLWidget::saveSpline()
{
    auto* splineToSave = new BSpline{splineDegree, bullets, knots};
    savedSplines.push_back(splineToSave);
}

void CurvesGLWidget::clearSplines()
{
   savedSplines.clear();
   update();
}

void CurvesGLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Поле ограничено линиями y = -0.3; y = 2.05; x = -2.05; x = 2.05

    const std::array<double, 3> COLOR_RED       {0.85, 0.34, 0.39};
    const std::array<double, 3> COLOR_ORANGE    {0.87, 0.44, 0.15};
    const std::array<double, 3> COLOR_YELLOW    {0.98, 0.95, 0.21};
    const std::array<double, 3> COLOR_GREEN     {0.42, 0.75, 0.19};
    const std::array<double, 3> COLOR_BLUE      {0.37, 0.80, 0.89};
    const std::array<double, 3> COLOR_DARK_BLUE {0.36, 0.43, 0.88};
    const std::array<double, 3> COLOR_MAGENTA   {0.25, 0.25, 0.45};

    plotBullet(bullets[0].x(), bullets[0].y(), COLOR_RED);
    plotBullet(bullets[1].x(), bullets[1].y(), COLOR_ORANGE);
    plotBullet(bullets[2].x(), bullets[2].y(), COLOR_YELLOW);
    plotBullet(bullets[3].x(), bullets[3].y(), COLOR_GREEN);
    plotBullet(bullets[4].x(), bullets[4].y(), COLOR_BLUE);
    plotBullet(bullets[5].x(), bullets[5].y(), COLOR_DARK_BLUE);
    plotBullet(bullets[6].x(), bullets[6].y(), COLOR_MAGENTA);
    for (const auto* savedSpline : savedSplines) {
        plotSpline(*savedSpline);
    }

    plotSplineWithKnots({splineDegree, bullets, knots});
}

void CurvesGLWidget::resizeKnots()
{
    knots.resize(bullets.size() + splineDegree + 1);
    knots.squeeze();

    int numberOfZeroValuedNodes{splineDegree + 1};
    int numberOfOneValuedNodes{splineDegree};
    int numberOfEqualyDestributedNodes{knots.size() - numberOfZeroValuedNodes - numberOfOneValuedNodes};

    for (int i = 0; i < numberOfZeroValuedNodes; ++i) {
        knots[i] = 0;
    }

    for (int i = 0; i < numberOfEqualyDestributedNodes; ++i) {
        float step = 1.0F / numberOfEqualyDestributedNodes;
        knots[i + numberOfZeroValuedNodes] = knots[i - 1 + numberOfZeroValuedNodes] + step;
    }

    for (int i = 0; i < numberOfOneValuedNodes; ++i) {
        knots[i + numberOfZeroValuedNodes + numberOfEqualyDestributedNodes] = 1;
    }

    emit knotsChanged();
}

void CurvesGLWidget::plotSpline(const BSpline& spline)
{
    glBegin(GL_LINE_STRIP);

//    for (double t = knots[splineDegree]; t < knots[knots.length() - splineDegree - 1]; t += 0.01) {
    for (float t = 0; t <= 1; t += 0.001F) {
        QVector3D splineEval = spline.eval(t);
        glColor3f(1 - t, 0.5, 0.5);
        glVertex2f(splineEval.x(), splineEval.y());
    }

    glEnd();
}

void CurvesGLWidget::plotSplineWithKnots(const BSpline &spline)
{
    static const double RADIUS = 0.02;
    for (float knot : knots) {
        QVector3D knotEval = spline.eval(knot);
        plotCircle(double(knotEval.x()), double(knotEval.y()), RADIUS);
    }

    plotSpline(spline);
}
