#ifndef TANGENTSGLWIDGET_H
#define TANGENTSGLWIDGET_H

#include "nicglwidget.h"
#include "../calculations/tangentscalculations.h"

class TangentsGLWidget : public NicGLWidget
{
    Q_OBJECT
public:
    TangentsGLWidget(QWidget* parent = nullptr);

protected:
    void paintGL();
    void plotTangent();

public slots:
    void plotCircles();
    void setX1(double value);
    void setY1(double value);
    void setR1(double value);
    void setX2(double value);
    void setY2(double value);
    void setR2(double value);

signals:
    void inner_first_tangent_x1_changed(double);
    void inner_first_tangent_x2_changed(double);
    void inner_first_tangent_y1_changed(double);
    void inner_first_tangent_y2_changed(double);

    void inner_second_tangent_x1_changed(double);
    void inner_second_tangent_x2_changed(double);
    void inner_second_tangent_y1_changed(double);
    void inner_second_tangent_y2_changed(double);

    void out_first_tangent_x1_changed(double);
    void out_first_tangent_x2_changed(double);
    void out_first_tangent_y1_changed(double);
    void out_first_tangent_y2_changed(double);

    void out_second_tangent_x1_changed(double);
    void out_second_tangent_x2_changed(double);
    void out_second_tangent_y1_changed(double);
    void out_second_tangent_y2_changed(double);

private:
    QMap<QString, QMap<QString, double>> data;
    TangentsCalculations arith_date;
};

#endif // TANGENTSGLWIDGET_H
