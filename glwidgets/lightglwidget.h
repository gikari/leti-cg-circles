#ifndef LIGHTGLWIDGET_H
#define LIGHTGLWIDGET_H

#include "nicglwidget.h"
#include "QWidget"

class LightGLWidget : public NicGLWidget
{
    Q_OBJECT
public:
    LightGLWidget(QWidget* parent = nullptr);

public slots:
    void setCameraPositionX(double value);
    void setCameraPositionY(double value);
    void setCameraPositionZ(double value);

    void setCameraRotationAngle(double angle);

    void setLightPositionX(double value);
    void setLightPositionY(double value);
    void setLightPositionZ(double value);

    void setLightIntensity(double value);

    void setLightRotationAngle(double angle);

signals:
    void lightPositionXUpdated(double value);
    void lightPositionYUpdated(double value);
    void lightPositionZUpdated(double value);

    void cameraPositionXUpdated(double value);
    void cameraPositionYUpdated(double value);
    void cameraPositionZUpdated(double value);

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

private:
    void plotStdandardCube();

    QVector3D cameraPosition;
    QVector3D lightPosition;

    float cameraRotationAngleAroundOY;
    float lightRotationAngleAroundOY;
    float lightIntensity;
};

#endif // LIGHTGLWIDGET_H
