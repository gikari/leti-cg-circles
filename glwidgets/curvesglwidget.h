#ifndef CURVESGLWIDGET_H
#define CURVESGLWIDGET_H

#include <QObject>
#include <QWidget>
#include <QVector3D>
#include "../calculations/bspline.h"
#include "nicglwidget.h"

class CurvesGLWidget : public NicGLWidget
{
    Q_OBJECT
public:
    CurvesGLWidget(QWidget* parent = nullptr);

    QVector3D& getBulletCoordinates(int bulletNumber) {
        return bullets[bulletNumber];
    }

    void setKnotsValue(int knotNumber, double knotValue) {
        knots[knotNumber] = float(knotValue);
        update();
    }

    float getKnotsValue(int knotNumber) {
        return knots[knotNumber];
    }

    QVector<float>& getKnots() {
        return knots;
    }

    void setBulletX(int bulletNumber, double value) {
        bullets[bulletNumber].setX(float(value));
        update();
    }

    void setBulletY(int bulletNumber, double value) {
        bullets[bulletNumber].setY(float(value));
        update();
    }

public slots:
    void setSplineDegree(int degree);
    void saveSpline();
    void clearSplines();

signals:
    void knotsChanged();

protected:
    void paintGL();
private:
    void resizeKnots();
    void plotSpline(const BSpline& spline);
    void plotSplineWithKnots(const BSpline& spline);

    QVector<QVector3D> bullets;
    int splineDegree;
    QVector<float> knots;
    QVector<const BSpline*> savedSplines;
};

#endif // CURVESGLWIDGET_H
