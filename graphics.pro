#-------------------------------------------------
#
# Project created by QtCreator 2019-02-13T23:15:59
#
#-------------------------------------------------

QT       += core gui opengl
LIBS 	 += -lglut -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = graphics
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
    glwidgets/lightglwidget.cpp \
        main.cpp \
        mainwindow.cpp \
    glwidgets/nicglwidget.cpp \
    tabs/lighttab.cpp \
    tabs/tangentstab.cpp \
    tabs/curvestab.cpp \
    glwidgets/tangentsglwidget.cpp \
    glwidgets/surfacesglwidget.cpp \
    calculations/tangentscalculations.cpp \
    calculations/bspline.cpp \
    calculations/bsplinesurface.cpp \
    glwidgets/curvesglwidget.cpp \
    tabs/surfacestab.cpp \
    tabs/clippingtab.cpp

HEADERS += \
    glwidgets/lightglwidget.h \
        mainwindow.h \
    glwidgets/nicglwidget.h \
    tabs/lighttab.h \
    tabs/tangentstab.h \
    tabs/curvestab.h \
    glwidgets/tangentsglwidget.h \
    calculations/tangentscalculations.h \
    glwidgets/surfacesglwidget.h \
    calculations/bspline.h \
    glwidgets/curvesglwidget.h \
    calculations/bsplinesurface.h \
    tabs/surfacestab.h \
    tabs/clippingtab.h

FORMS += \
        mainwindow.ui \
    tabs/lighttab.ui \
    tabs/tangentstab.ui \
    tabs/curvestab.ui \
    tabs/surfacestab.ui \
    tabs/clippingtab.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
