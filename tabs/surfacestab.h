#ifndef SURFACESTAB_H
#define SURFACESTAB_H

#include <QWidget>
#include <QPoint>

namespace Ui {
class SurfacesTab;
}

class SurfacesTab : public QWidget
{
    Q_OBJECT

public:
    explicit SurfacesTab(QWidget *parent = nullptr);
    ~SurfacesTab();

public slots:
    void fillPolyhedronTable();
    void updatePointCoordinates(int row, int column);

    void updateVertexX(double value);
    void updateVertexY(double value);
    void updateVertexZ(double value);

signals:
    void updateVertexX(double value, QPoint vertexIndexes);
    void updateVertexY(double value, QPoint vertexIndexes);
    void updateVertexZ(double value, QPoint vertexIndexes);

private:
    Ui::SurfacesTab *ui;
};

#endif // SURFACESTAB_H
