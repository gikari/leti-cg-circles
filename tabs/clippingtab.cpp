#include "clippingtab.h"
#include "ui_clippingtab.h"
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QDebug>
#include <QString>

ClippingTab::ClippingTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClippingTab)
{
    ui->setupUi(this);
    ui->table_rectangle->setCurrentCell(0, 0);
    ui->table_window->setCurrentCell(0, 0);
}

ClippingTab::~ClippingTab()
{
    delete ui;
}

void ClippingTab::addPointToRectangleTable()
{
    addRowToTable(ui->table_rectangle);
    // TODO add row to model
}

void ClippingTab::removePointFromRectangleTable()
{
    removeRowFromTable(ui->table_rectangle);
    // TODO remove row from model
}

void ClippingTab::addPointToWindowTable()
{
    addRowToTable(ui->table_window);
    // TODO add row to model
}

void ClippingTab::removePointFromWindowTable()
{
    removeRowFromTable(ui->table_window);
    // TODO remove row from model
}

void ClippingTab::addRowToTable(QTableWidget *table)
{
    int currentRow = table->currentRow();
    table->insertRow(currentRow + 1);
}

void ClippingTab::removeRowFromTable(QTableWidget *table)
{
    int currentRow = table->currentRow();
    table->removeRow(currentRow);
}

