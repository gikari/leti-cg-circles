#ifndef CURVESTAB_H
#define CURVESTAB_H

#include <QWidget>

namespace Ui {
class CurvesTab;
}

class CurvesTab : public QWidget
{
    Q_OBJECT

public:
    explicit CurvesTab(QWidget *parent = nullptr);
    ~CurvesTab();

public slots:
    void setKnotValue();
    void updateKnotsValue();
    void updateKnotsList();
    void updateBulletsCoordinates(int bulletNumber);
    void setBulletX(double value);
    void setBulletY(double value);

private:
    Ui::CurvesTab *ui;
};

#endif // CURVESTAB_H
