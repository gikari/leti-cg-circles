#include "surfacestab.h"
#include "ui_surfacestab.h"

#include <QPoint>
#include <QVector>
#include <QVector3D>
#include <QTableWidgetItem>

SurfacesTab::SurfacesTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SurfacesTab)
{
    ui->setupUi(this);
    fillPolyhedronTable();
    ui->polyhydron_table->setCurrentCell(0, 0);
}

SurfacesTab::~SurfacesTab()
{
    delete ui;
}

void SurfacesTab::fillPolyhedronTable()
{
    QVector<QVector<QVector3D>> polyhedron = ui->gl_widget->getPolyheron();

    ui->polyhydron_table->setRowCount(0);
    ui->polyhydron_table->setColumnCount(0);
    for (int i = 0; i < polyhedron.size(); ++i) {
        ui->polyhydron_table->insertRow(i);
        for (int j = 0; j < polyhedron[i].size(); ++j) {
            if (ui->polyhydron_table->columnCount() < j + 1) {
                ui->polyhydron_table->insertColumn(j);
            }
            QTableWidgetItem* item = new QTableWidgetItem();
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            ui->polyhydron_table->setItem(i, j, item);
        }

    }
}

void SurfacesTab::updatePointCoordinates(int row, int column)
{
    QVector3D point = ui->gl_widget->getPolyhedronPointAt(row, column);

    ui->point_x_box->blockSignals(true);
    ui->point_y_box->blockSignals(true);
    ui->point_z_box->blockSignals(true);

    ui->point_x_box->setValue(qreal(point.x()));
    ui->point_y_box->setValue(qreal(point.y()));
    ui->point_z_box->setValue(qreal(point.z()));

    ui->point_x_box->blockSignals(false);
    ui->point_y_box->blockSignals(false);
    ui->point_z_box->blockSignals(false);
}

void SurfacesTab::updateVertexX(double value)
{
    int vertexUIndex = ui->polyhydron_table->currentRow();
    int vertexWIndex = ui->polyhydron_table->currentColumn();
    QPoint vertexIndexes{vertexUIndex, vertexWIndex};

    emit updateVertexX(value, vertexIndexes);
}

void SurfacesTab::updateVertexY(double value)
{
    int vertexUIndex = ui->polyhydron_table->currentRow();
    int vertexWIndex = ui->polyhydron_table->currentColumn();
    QPoint vertexIndexes{vertexUIndex, vertexWIndex};

    emit updateVertexY(value, vertexIndexes);
}

void SurfacesTab::updateVertexZ(double value)
{
    int vertexUIndex = ui->polyhydron_table->currentRow();
    int vertexWIndex = ui->polyhydron_table->currentColumn();
    QPoint vertexIndexes{vertexUIndex, vertexWIndex};

    emit updateVertexZ(value, vertexIndexes);
}
