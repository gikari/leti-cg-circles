#ifndef LIGHTTAB_H
#define LIGHTTAB_H

#include <QWidget>

namespace Ui {
class LightTab;
}

class LightTab : public QWidget
{
    Q_OBJECT

public:
    explicit LightTab(QWidget *parent = nullptr);
    ~LightTab();

private:
    Ui::LightTab *ui;
};

#endif // LIGHTTAB_H
