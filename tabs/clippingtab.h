#ifndef CLIPPINGTAB_H
#define CLIPPINGTAB_H

#include <QWidget>
#include <QTableWidget>

namespace Ui {
class ClippingTab;
}

class ClippingTab : public QWidget
{
    Q_OBJECT

public:
    explicit ClippingTab(QWidget *parent = nullptr);
    ~ClippingTab();

public slots:
    void addPointToRectangleTable();
    void removePointFromRectangleTable();

    void addPointToWindowTable();
    void removePointFromWindowTable();
private:
    void addRowToTable(QTableWidget *table);
    void removeRowFromTable(QTableWidget *table);

    Ui::ClippingTab *ui;
};

#endif // CLIPPINGTAB_H
