#include "tangentstab.h"
#include "ui_tangentstab.h"

TangentsTab::TangentsTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TangentsTab)
{
    ui->setupUi(this);
}

TangentsTab::~TangentsTab()
{
    delete ui;
}


void TangentsTab::inner_first_tangent_x1_changed(double val)
{
    ui->xit1_1_le->setText(QString::number(val));
}

void TangentsTab::inner_first_tangent_x2_changed(double val)
{
    ui->xit1_2_le->setText(QString::number(val));
}

void TangentsTab::inner_first_tangent_y1_changed(double val)
{
    ui->yit1_1_le->setText(QString::number(val));
}

void TangentsTab::inner_first_tangent_y2_changed(double val)
{
    ui->yit1_2_le->setText(QString::number(val));
}

void TangentsTab::inner_second_tangent_x1_changed(double val)
{
    ui->xit2_1_le->setText(QString::number(val));
}

void TangentsTab::inner_second_tangent_x2_changed(double val)
{
    ui->xit2_2_le->setText(QString::number(val));
}

void TangentsTab::inner_second_tangent_y1_changed(double val)
{
    ui->yit2_1_le->setText(QString::number(val));
}

void TangentsTab::inner_second_tangent_y2_changed(double val)
{
    ui->yit2_2_le->setText(QString::number(val));
}


void TangentsTab::out_first_tangent_x1_changed(double val)
{
    ui->xot1_1_le->setText(QString::number(val));
}

void TangentsTab::out_first_tangent_x2_changed(double val)
{
    ui->xot1_2_le->setText(QString::number(val));
}

void TangentsTab::out_first_tangent_y1_changed(double val)
{
    ui->yot1_1_le->setText(QString::number(val));
}

void TangentsTab::out_first_tangent_y2_changed(double val)
{
    ui->yot1_2_le->setText(QString::number(val));
}

void TangentsTab::out_second_tangent_x1_changed(double val)
{
    ui->xot2_1_le->setText(QString::number(val));
}

void TangentsTab::out_second_tangent_x2_changed(double val)
{
    ui->xot2_2_le->setText(QString::number(val));
}

void TangentsTab::out_second_tangent_y1_changed(double val)
{
    ui->yot2_1_le->setText(QString::number(val));
}

void TangentsTab::out_second_tangent_y2_changed(double val)
{
    ui->yot2_2_le->setText(QString::number(val));
}
