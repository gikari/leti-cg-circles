#ifndef TANGENTSTAB_H
#define TANGENTSTAB_H

#include <QWidget>

namespace Ui {
class TangentsTab;
}

class TangentsTab : public QWidget
{
    Q_OBJECT

public:
    explicit TangentsTab(QWidget *parent = nullptr);
    ~TangentsTab();

public slots:
    void inner_first_tangent_x1_changed(double val);
    void inner_first_tangent_x2_changed(double val);
    void inner_first_tangent_y1_changed(double val);
    void inner_first_tangent_y2_changed(double val);

    void inner_second_tangent_x1_changed(double val);
    void inner_second_tangent_x2_changed(double val);
    void inner_second_tangent_y1_changed(double val);
    void inner_second_tangent_y2_changed(double val);

    void out_first_tangent_x1_changed(double val);
    void out_first_tangent_x2_changed(double val);
    void out_first_tangent_y1_changed(double val);
    void out_first_tangent_y2_changed(double val);

    void out_second_tangent_x1_changed(double val);
    void out_second_tangent_x2_changed(double val);
    void out_second_tangent_y1_changed(double val);
    void out_second_tangent_y2_changed(double val);

private:
    Ui::TangentsTab *ui;
};

#endif // TANGENTSTAB_H
