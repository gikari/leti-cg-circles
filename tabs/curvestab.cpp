#include "curvestab.h"
#include "ui_curvestab.h"

CurvesTab::CurvesTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CurvesTab)
{
    ui->setupUi(this);
    updateBulletsCoordinates(0);
    updateKnotsList();
}

CurvesTab::~CurvesTab()
{
    delete ui;
}

void CurvesTab::setKnotValue()
{
    int knotNumber = ui->knot_number_box->currentIndex();
    double knotValue = ui->knot_value_box->value();
    ui->openGLWidget->setKnotsValue(knotNumber, knotValue);
}

void CurvesTab::updateKnotsValue()
{
    int knotNumber = ui->knot_number_box->currentIndex();
    float knotValue = ui->openGLWidget->getKnotsValue(knotNumber);
    ui->knot_value_box->setValue(double(knotValue));
}

void CurvesTab::updateKnotsList()
{
   QVector<float> newKnots = ui->openGLWidget->getKnots();
   ui->knot_number_box->blockSignals(true);
   ui->knots_list->blockSignals(true);
   ui->knot_number_box->clear();
   ui->knots_list->clear();
   for (int i = 0; i < newKnots.size(); i++) {
       ui->knot_number_box->addItem(QString::number(i));
       ui->knots_list->addItem(QString::number(double(newKnots[i])));
   }
   ui->knot_number_box->blockSignals(false);
   ui->knots_list->blockSignals(false);
   updateKnotsValue();
}

void CurvesTab::updateBulletsCoordinates(int bulletNumber)
{
    QVector3D bulletCoordinates = ui->openGLWidget->getBulletCoordinates(bulletNumber);
    ui->x_box->setValue(double(bulletCoordinates.x()));
    ui->y_box->setValue(double(bulletCoordinates.y()));
}

void CurvesTab::setBulletX(double value)
{
   int bulletNumber = ui->point_number_box->currentIndex();
   ui->openGLWidget->setBulletX(bulletNumber, value);
}

void CurvesTab::setBulletY(double value)
{
   int bulletNumber = ui->point_number_box->currentIndex();
   ui->openGLWidget->setBulletY(bulletNumber, value);
}
