#ifndef TANGENTSCALCULATIONS_H
#define TANGENTSCALCULATIONS_H

#include <QMap>


struct circle
{
    circle() {}
    int number_center_point;    // Порядковый номер точки в point_matrix
    double radius;              // Радиус окружности
};

class TangentsCalculations
{
public:
    TangentsCalculations();
    ~TangentsCalculations();

    /**
     * @brief give_x_point даёт копию значения координаты Y точки number.
     * @param number номер точки, возвращаемый при её создании.
     * @return значение Y точки number.
     */
    double give_y_point(int number);

    /**
     * @brief give_x_point даёт копию значения координаты Х точки number.
     * @param number номер точки, возвращаемый при её создании.
     * @return значение Х точки number.
     */
    double give_x_point(int number);

    /**
     * @brief change_circle изменяет параметры созданной до этого окружности.
     * @param numbre номер окружности, выданный при создании окружности.
     * @param x новые координаты центра.
     * @param y новые координаты центра.
     * @param radius новое значение радиуса.
     */
    void change_circle(int number, double x, double y, double radius);

    /**
     * @brief change_point изменяет координаты точки с заданным номером.
     * @param number номер точки возвращённый при создании точки.
     * @param x новая координата точки.
     * @param y новая координата точки.
     */
    void change_point(int number, double x, double y);

    /**
     * @brief create_new_point_2d добавляет в конце массива координаты точки.
     * @param x - значение X создаваемой точки.
     * @param y - значение Y создаваемой точки.
     * @return возвращает порядковый номер созданной точки.
     */
    int create_new_point_2d(double x, double y);

    /**
     * @brief create_new_circle_2d - создаёт точку в point_matrix и назначет эту точку центром окружности с радиусом radius.
     * @param x - координата X центра окружности.
     * @param y - координата Y центра окружности.
     * @param radius - радиус окружности.
     * @return порядковый номер окружности в circle_array.
     */
    int create_new_circle_2d(double x, double y, double radius);

    /**
     * @brief create_new_circle_2d - назначет точку с номером number_center центром окружности с радиусом radius.
     * @param number_center - порядковый номер точки в point_matrix.
     * @param radius - радиус окружности.
     * @return порядковый номер окружности в circle_array.
     */
    int create_new_circle_2d(int number_center, double radius);

    /**
     * @brief delete_last_point удаляет последнюю созданную точку.
     */
    void delete_last_point();

    /**
     * @brief pop_element выдаёт последнюю созданную точку, с последующим её удалением.
     * @return Возвращает QMap с ключами "x" и "y", которым хранятся значения x и y точки.
     */
    QMap<QString, double> pop_element();

    /**
     * @brief rotate_2d поворот всей фигуры вокруг центра координат на угол alfa_in_radians.
     * @param alfa_in_radians угол на который повернуть объект.
     */
    void rotate_2d(double alfa_in_radians);

    /**
     * @brief displace_2d - задаёт локальную систему координат параллельную осям координат в точке (x;y).
     * @param x координата центра для локальной системы координат.
     * @param y координата центра для локальной системы координат.
     */
    void displace_2d(double x, double y);

    /**
     * @brief rotation_around_point_2d поворачивает объект вокруг заданой точки (x;y) на угол alfa_in_radians.
     * @param alfa_in_radians угол поворота в радианах.
     * @param x координата точки, центра поворота.
     * @param y координата точки, центра поворота.
     */
    void rotation_around_point_2d(double alfa_in_radians, double x, double y);

    /**
     * @brief set_new_coordinate_sys_2d передвигает и поворачивает ось кординат в точку (x;y) и на угол alfa_in_radians.
     * @param alfa_in_radians угол между прямой и осью Ox.
     * @param x координата точки, центра новой системы координат.
     * @param y координата точки, центра новой системы координат.
     */
    void set_new_coordinate_sys_2d(double alfa_in_radians, double x, double y);

    /**
     * @brief remove_new_coordinate_sys_2d возвращает ось кординат из точки (x;y) и с угол alfa_in_radians.
     * @param alfa_in_radians бывший угол между прямой и осью Ox.
     * @param x координата точки, центра новой системы координат.
     * @param y координата точки, центра новой системы координат.
     */
    void remove_new_coordinate_sys_2d(double alfa_in_radians, double x, double y);

    /**
     * @brief tangent_circles_2d находит точки касания внешних касательных к двум окружностям
     *  с номерами number_first_circle и number_second_circle, записывая ответ в point_matrix.
     * @param number_first_circle - номер в circle_array первой окружности.
     * @param number_second_circle - номер в circle_array второй окружности.
     * @return количество точек в point_matrix.
     */
    int tangent_circles_2d(int number_first_circle, int number_second_circle);

   /**
     * @brief trase_2d
     * Функция находит и добавляет в массив точек,
     * точку лежащую на прямой, проведённой через точки
     * с номерами first_point_number и second_point_number
     * и находящуюся на растоянии distance от точки с номером second_point_number.
     * @param x_first_point координата Х первой точки.
     * @param y_first_point координата Y первой точки.
     * @param x_second_point координата Х второй точки.
     * @param y_second_point координата Y второй точки.
     * @param distance - расстояние по прямой от second_point_number до новой точки.
     *                     При положительном значении берётся растояние не в сторону
     *                     first_point_number. При отрицательном значении берётся растояние
     *                     в сторону first_point_number.
     * @return порядковый номер созданной точки или 0 при неудаче.
     */
    int trase_2d(double x_first_point, double y_first_point, double x_second_point, double y_second_point, double distance);

    /**
     * @brief segment_into_line возвращает значения двух точек лежащих на продолжении пути от first_point_number
     *  в second_point_number (и наоборот) на растоянии distance.
     * @param first_point_number идентификатор первой точки, возрващаемый при создании точки.
     * @param second_point_number идентификатор второй точки, возрващаемый при создании точки.
     * @param distance длинна линии найденого пути.
     * @return ключи: "first-point" и "second-point" содержат в себе ключи: "x" "y"
     */
    QMap<QString, QMap<QString, double>> segment_into_line(double x_first_point, double y_first_point, double x_second_point, double y_second_point, double distance);

    /**
     * @brief change_accuracy меняет порядок точности косания окружностей.
     * @param exponent новый порядок точности.
     */
    void change_accuracy(int exponent);

    void test_debug();

    void points_view_debug();
private:

    /**
     * @brief custom_transform_2d
     * Производит умножение справа всей матрицы точек и переданной матрицы.
     * Записывая новые значения в матрицу точек. Если столбец H оказывается не равный 1,
     * то производит деление всей строки, так чтобы H было равно 1.
     * @param transform_matrix - матрица преобразования. Предполагается, что матрицы 3 на 3.
     *
     * @return void
     */
    void custom_transform_2d(double** transform_matrix);

    /**
     * @brief custom_transform_2d
     * Производит умножение справа части матрицы точек, ограниченной high_border и low_border,
     * и переданной матрицы.
     * Записывая новые значения в матрицу точек. Если столбец H оказывается не равный 1,
     * то производит деление всей строки, так чтобы H было равно 1.
     * @param transform_matrix - матрица преобразования. Предполагается, что матрицы 3 на 3.
     * @param high_border - меньшая граница необходимых точек. Должна быть не больше low_border.
     * @param low_border - большая граница необходимых точек. Должна быть не меньше high_border.
     */
    void custom_transform_2d(double** transform_matrix, int high_border, int low_border);




    const int max_count_point = 150;

    double* point_matrix[3] = {nullptr};    // Хранение всех точек перевёрнуто
                                            // Массив будет содержать 3 строки, а не 3 столбца
    circle* circle_array = nullptr;

    int count_of_point = -1;
    int count_of_circle = -1;

    double out_tangent_array[4][2];         // Матрица точек косания

    double accuracy = 0.001;


};
#endif // TANGENTCALCULATIONS_H
