#include "bsplinesurface.h"
#include "bspline.h"
#include <QRandomGenerator>
#include <QMatrix4x4>
#include <QtMath>
#include <cmath>

BSplineSurface::BSplineSurface() :
    polyhedron{},
    knotsU{
        0, 0, 0, 0,
        0.5, 0.6F, 0.7F,
        1, 1, 1, 1
    },
    knotsW{
        0, 0, 0, 0,
        0.3F, 0.6F, 0.9F,
        1, 1, 1, 1
    },
    degreeU{2},
    degreeW{2}
{
    generatePolyhedron(this->polyhedron, 4, 4);
}

BSplineSurface::BSplineSurface(QVector<QVector<QVector3D>> &polyhedron,
                               int degreeU,
                               int degreeW) :
    polyhedron{polyhedron},
    knotsU{},
    knotsW{},
    degreeU{degreeU},
    degreeW{degreeW}
{
    resizeKnotVector(knotsU, polyhedron.size(), degreeU);
    resizeKnotVector(knotsW, polyhedron[0].size(), degreeW);
}

QVector3D BSplineSurface::eval(float valueU, float valueW)
{
    QVector3D result{};
    for (int i = 0; i < polyhedron.size(); ++i) {
        QVector<QVector3D> controlPointsU = polyhedron[i];
        BSpline splineU{degreeU, controlPointsU, knotsU};
        float basisFunctionResultU = splineU.evalBasisFunction(valueU, i, degreeU);

        QVector3D innerSum{};
        for (int j = 0; j < polyhedron[i].size(); ++j) {
            QVector<QVector3D> controlPointsW = columnVectorOfMatrix(polyhedron, j);
            BSpline splineW{degreeW, controlPointsW, knotsW};
            float basisFunctionResultW = splineW.evalBasisFunction(valueW, j, degreeW);

            innerSum += basisFunctionResultW * polyhedron[i][j];
        }

        result += basisFunctionResultU * innerSum;
    }
    return result;
}

const QVector<QVector<QVector3D> > &BSplineSurface::getPolyheron() const {
    return polyhedron;
}

QVector3D &BSplineSurface::getPolyhedronPointAt(int row, int column)
{
    return polyhedron[row][column];
}

void BSplineSurface::transformWithMatrix(const QMatrix4x4& matrix)
{
    for (QVector<QVector3D>& row : polyhedron) {
        for (QVector3D& point : row) {
            point = matrix.mapVector(point);
        }
    }
}

void BSplineSurface::generatePolyhedron(QVector<QVector<QVector3D>> &polyhedron,
                                        int width,
                                        int height)
{
    polyhedron.reserve(height);
    for (int i = 0; i < height; ++i) {
        polyhedron.push_back({});
        polyhedron[i].reserve(width);
        for (int j = 0; j < width; ++j) {
            // Random between -2 and 2
            qreal randomX = -2 + QRandomGenerator::global()->bounded(4.0);
            qreal randomY = -2 + QRandomGenerator::global()->bounded(4.0);
            qreal randomZ = -2 + QRandomGenerator::global()->bounded(4.0);

            polyhedron[i].push_back({float(randomX),
                                     float(randomY),
                                     float(randomZ)
                                    });
        }
    }
}

void BSplineSurface::rotateAroundOX(float angle)
{
    angle = qDegreesToRadians(angle);
    QMatrix4x4 transformMatrix{
        1,                0,               0, 0,
        0,  std::cos(angle), std::sin(angle), 0,
        0, -std::sin(angle), std::cos(angle), 0,
        0,                0,               0, 1
    };
    transformWithMatrix(transformMatrix);
}

void BSplineSurface::rotateAroundOY(float angle)
{
    angle = qDegreesToRadians(angle);
    QMatrix4x4 transformMatrix{
        std::cos(angle), 0, -std::sin(angle), 0,
                      0, 1,                0, 0,
        std::sin(angle), 0,  std::cos(angle), 0,
                      0, 0,                0, 1
    };
    transformWithMatrix(transformMatrix);
}

void BSplineSurface::rotateAroundOZ(float angle)
{
    angle = qDegreesToRadians(angle);
    QMatrix4x4 transformMatrix{
         std::cos(angle), std::sin(angle), 0, 0,
        -std::sin(angle), std::cos(angle), 0, 0,
                       0,               0, 1, 0,
                       0,               0, 0, 1
    };
    transformWithMatrix(transformMatrix);
}

void BSplineSurface::updatePointX(float value, QPoint pointIndexes)
{
    polyhedron[pointIndexes.x()][pointIndexes.y()].setX(value);
}

void BSplineSurface::updatePointY(float value, QPoint pointIndexes)
{
    polyhedron[pointIndexes.x()][pointIndexes.y()].setY(value);
}

void BSplineSurface::updatePointZ(float value, QPoint pointIndexes)
{
    polyhedron[pointIndexes.x()][pointIndexes.y()].setZ(value);
}

void BSplineSurface::setDegreeU(int degree)
{
    degreeU = degree;
}

void BSplineSurface::setDegreeW(int degree)
{
    degreeW = degree;
}

void BSplineSurface::resizeKnotVector(QVector<float> &knotVector,
                                      int controlPointsSize,
                                      int splineDegree)
{
    knotVector.resize(controlPointsSize + splineDegree + 1);
    knotVector.squeeze();

    int numberOfZeroValuedNodes{splineDegree + 1};
    int numberOfOneValuedNodes{splineDegree};
    int numberOfEqualyDestributedNodes{
        knotVector.size() - numberOfZeroValuedNodes - numberOfOneValuedNodes};

    for (int i = 0; i < numberOfZeroValuedNodes; ++i) {
        knotVector[i] = 0;
    }

    for (int i = 0; i < numberOfEqualyDestributedNodes; ++i) {
        float step = 1.0F / numberOfEqualyDestributedNodes;
        knotVector[i + numberOfZeroValuedNodes] = knotVector[i - 1 + numberOfZeroValuedNodes] + step;
    }

    for (int i = 0; i < numberOfOneValuedNodes; ++i) {
        knotVector[i + numberOfZeroValuedNodes + numberOfEqualyDestributedNodes] = 1;
    }
}

QVector<QVector3D> BSplineSurface::columnVectorOfMatrix(QVector<QVector<QVector3D> > matrix, int column)
{
    QVector<QVector3D> result{};
    result.reserve(matrix.size());
    for (int i = 0; i < matrix.size(); ++i) {
        result.push_back(matrix[i][column]);
    }
    return result;
}


