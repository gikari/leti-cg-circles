#ifndef BSPLINE_H
#define BSPLINE_H

#include "bsplinesurface.h"
#include <QVector>
#include <QPointF>
#include <QVector3D>

class BSpline
{
public:
    BSpline(int degree, QVector<QVector3D>& points, QVector<float>& knots);

    QVector3D eval(float point) const;
    QVector<float> getKnots() {
        return knots;
    }

    static bool fequals(float a, float b);

private:
    float evalBasisFunctionFirstDegree(float knot, int i) const;
    float evalBasisFunction(float knot, int i, int deg) const;

    QVector<QVector3D> points;
    QVector<float> knots;
    int degree;

    friend BSplineSurface;
};

#endif // BSPLINE_H
