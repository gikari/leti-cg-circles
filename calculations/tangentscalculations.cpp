#include "tangentscalculations.h"
#include <cmath>
#include <QDebug>
#include <QMap>

TangentsCalculations::TangentsCalculations()
{
    for(int i = 0; i < 3; i++)
    {
        point_matrix[i] = new double[max_count_point];
    }

    circle_array = new circle[25];
}

TangentsCalculations::~TangentsCalculations()
{
    for (int i = 0; i < 3; i++)
    {
        delete[] point_matrix[i];
    }
    delete[] circle_array;
}

double TangentsCalculations::give_y_point(int number)
{
    return point_matrix[1][number];
}

double TangentsCalculations::give_x_point(int number)
{
    return point_matrix[2][number];
}

void TangentsCalculations::change_circle(int number, double x, double y, double radius)
{
//    qDebug() << "OLD COORDINATE of circle" << point_matrix[2][circle_array[number].number_center_point] << point_matrix[1][circle_array[number].number_center_point] << circle_array[number].radius;
//    qDebug() << "NEW COORDINATE of circle" << x << y << radius;
    circle_array[number].radius = radius;

    change_point(circle_array[number].number_center_point, x, y);

    point_matrix[1][circle_array[number].number_center_point] = y;
    point_matrix[2][circle_array[number].number_center_point] = x;
}

void TangentsCalculations::change_point(int number, double x, double y)
{
//    qDebug() << "OLD COORDINATE of POINT" << point_matrix[2][number] << point_matrix[1][number];
//    qDebug() << "NEW COORDINATE of POINT" << x << y;
    point_matrix[1][number] = y;
    point_matrix[2][number] = x;

//    qDebug() << "NEW COORDINATE afte change of POINT" << point_matrix[2][number] << point_matrix[1][number];

}

void TangentsCalculations::custom_transform_2d(double** transform_matrix, int high_border, int low_border)
{
//    qDebug() << "point: BEGIN custom_transform_2d";

    points_view_debug();

    // Временные хранилища посчитанных значений для одной точки
    double buffer_x;
    double buffer_y;
    double buffer_H;

    for(int i = high_border; i <= low_border; i++)
    {
        buffer_x = 0;
        buffer_y = 0;
        buffer_H = 0;
        for(int k = 0; k < 3; k++)
        {
            buffer_x += point_matrix[3 - k - 1][i] * transform_matrix[k][0];
//            qDebug() << "buffer_x += " << point_matrix[2][i] << "*" << transform_matrix[k][0];
            buffer_y += point_matrix[3 - k - 1][i] * transform_matrix[k][1];
//            qDebug() << "               buffer_y += " << point_matrix[1][i] << "*" << transform_matrix[k][1];
            buffer_H += point_matrix[3 - k - 1][i] * transform_matrix[k][2];
//            qDebug() << "                               buffer_H += " << point_matrix[0][i] << "*" << transform_matrix[k][2];
        }
        if (buffer_H != 1)
        {
            buffer_x /= buffer_H;
            buffer_y /= buffer_H;
            buffer_H = 1;
        }
        point_matrix[2][i] = buffer_x;
        point_matrix[1][i] = buffer_y;
        point_matrix[0][i] = buffer_H;
    }
//    qDebug() << "point: END custom_transform_2d";
    points_view_debug();

}

void TangentsCalculations::rotate_2d(double alfa_in_radians)
{
//    qDebug() << "point: begin rotate_2d";
    double** matrix = new double*[3];

    for (int i = 0; i < 3; i++)
    {
        matrix[i] = new double[3];
        matrix[i][0] = 0;
        matrix[i][1] = 0;
        matrix[i][2] = 0;
    }

//    qDebug() << "point: alfa_in_radians = " << alfa_in_radians;
    double cos_alfa = cos(alfa_in_radians);
    double sin_alfa = sin(alfa_in_radians);
//    qDebug() << "point: cos(alfa_in_radians) = " << cos_alfa;
//    qDebug() << "point: sin(alfa_in_radians) = " << sin_alfa;
    matrix[0][0] = cos_alfa;
    matrix[0][1] = sin_alfa;
    matrix[1][0] = -sin_alfa;
    matrix[1][1] = cos_alfa;
    matrix[2][2] = 1;
    custom_transform_2d(matrix);

    for (int i = 0; i < 3; i++)
    {
//        qDebug() << "rotate matrix " << matrix[i][0] << matrix[i][1] << matrix[i][2] << endl;
        delete[] matrix[i];
    }
    delete[] matrix;
//    qDebug() << "point:end rotate_2d";
}

void TangentsCalculations::displace_2d(double x, double y)
{
//    qDebug() << "point: begin displace_2d";
    double** matrix = new double*[3];

    for (int i = 0; i < 3; i++)
    {
        matrix[i] = new double[3];
        matrix[i][0] = 0;
        matrix[i][1] = 0;
        matrix[i][2] = 0;
    }

    matrix[0][0] = 1;
    matrix[1][1] = 1;
    matrix[2][2] = 1;
    matrix[2][0] = -x;
    matrix[2][1] = -y;

    custom_transform_2d(matrix);

    for (int i = 0; i < 3; i++)
    {
        delete[] matrix[i];
    }
    delete[] matrix;

//    qDebug() << "point:end displace_2d";
}

void TangentsCalculations::rotation_around_point_2d(double alfa_in_radians, double x, double y)
{
    displace_2d(x, y);
    rotate_2d(alfa_in_radians);
    displace_2d(-x, -y);
}

void TangentsCalculations::set_new_coordinate_sys_2d(double alfa_in_radians, double x, double y)
{
    displace_2d(x, y);
    rotate_2d(-alfa_in_radians);
}

void TangentsCalculations::remove_new_coordinate_sys_2d(double alfa_in_radians, double x, double y)
{
    rotate_2d(alfa_in_radians);
    displace_2d(-x, -y);
}

int TangentsCalculations::tangent_circles_2d(int number_first_circle, int number_second_circle)
{
    //-----preparation begin
    int count_tangent_point_out = 0;

    int number_big_circle;
    int number_small_circle;

    double alfa;

//    qDebug() << "point: begin tangent_circles_2d" << circle_array[number_first_circle].radius << circle_array[number_second_circle].radius;
    if (circle_array[number_first_circle].radius > circle_array[number_second_circle].radius)
    {
        number_big_circle = number_first_circle;
        number_small_circle = number_second_circle;

    } else {

        number_big_circle = number_second_circle;
        number_small_circle = number_first_circle;

    }


    double y_circle_small = point_matrix[1][circle_array[number_small_circle].number_center_point];
    double x_circle_small = point_matrix[2][circle_array[number_small_circle].number_center_point];

    double y_circle_big = point_matrix[1][circle_array[number_big_circle].number_center_point];
    double x_circle_big = point_matrix[2][circle_array[number_big_circle].number_center_point];


//    qDebug() << "SMALL circle: x = " << x_circle_small << "; y = " << y_circle_small << endl;

//    qDebug() << "BIG circle: x = " << x_circle_big << "; y = " << y_circle_big << endl;


    double ankathete = x_circle_big - x_circle_small;       // Прилижащий
    double gegenkathete = y_circle_big - y_circle_small;    // Противолежащий

//    qDebug() << "point: ankathete = " << ankathete << "; gegenkathete = " << gegenkathete;

    double arg_atan;

    if (gegenkathete == 0 && ankathete == 0)
    {
        arg_atan = 0;
    }
    else
    {
        if (ankathete == 0)
        {
            arg_atan = gegenkathete > 0 ? 5000 : -5000;
        }
        else
        {
            arg_atan = gegenkathete/ankathete;
        }
    }
//    qDebug() << "point: begin atan" << arg_atan;
    alfa = atan(arg_atan);
//    qDebug() << "point: end atan" << alfa;

    if (ankathete < 0)
    {
        alfa += M_PI;
//        qDebug() << "POINT: alfa + PI" << alfa;
    }

//    qDebug() << "point: pre begin set_new_coordinate_sys_2d";
    double x_coordinate_center = x_circle_small;
    double y_coordinate_center = y_circle_small;
    set_new_coordinate_sys_2d(alfa, x_coordinate_center, y_coordinate_center);
//    qDebug() << "point: end set_new_coordinate_sys_2d";

    //-----preparation is over
    double radius_small  = circle_array[number_small_circle].radius;
    double radius_big  = circle_array[number_big_circle].radius;

    double delta_radius = radius_big - radius_small;
    double sum_radius = radius_big + radius_small;

    double distanc_centers = point_matrix[2][circle_array[number_big_circle].number_center_point];


//    qDebug() << "POINT: delta_radius = " << delta_radius << "; distanc_centers = " << distanc_centers;




    if (abs(delta_radius - distanc_centers) < accuracy)// The condition of tangency circle
    {
            create_new_point_2d(-radius_small, radius_small);
            count_tangent_point_out++;
            create_new_point_2d(-radius_small, 0);
            count_tangent_point_out++;
    }
    else if(delta_radius > distanc_centers)
    {
//        qDebug() << "point: Imposible to build a tangent. delta_radius = " << delta_radius << "; distanc_centers = " << distanc_centers;
        // Imposible to build a tangent
    } else
    {
        double delta_x;
        double delta_y;

        delta_x = radius_big * delta_radius / distanc_centers;

        double square_big_radius = radius_big*radius_big;
        double square_delta_x = delta_x*delta_x;
        delta_y = sqrt(square_big_radius - square_delta_x); // Pythagorean theorem

        create_new_point_2d(distanc_centers - delta_x, delta_y);
        count_tangent_point_out++;

        double ratio = radius_small / radius_big;

        create_new_point_2d(-(ratio * delta_x), ratio * delta_y);
        count_tangent_point_out++;

        create_new_point_2d(distanc_centers - delta_x, -delta_y);
        count_tangent_point_out++;

        create_new_point_2d(-(ratio * delta_x), -ratio * delta_y);
        count_tangent_point_out++;

        // inner tangent

//        qDebug() << "point: inner tangent sum_radius = " << sum_radius << "; distanc_centers = " << distanc_centers;

        if (abs(sum_radius - distanc_centers) < accuracy)
        {
//            qDebug() << "point: circle out touch";
            create_new_point_2d(radius_small, radius_small);
            count_tangent_point_out++;
            create_new_point_2d(radius_small, 0);
            count_tangent_point_out++;
        } else if(sum_radius < distanc_centers)
        {
//            qDebug() << "point: normal inner tangent";
            delta_x = radius_big * sum_radius / distanc_centers;
            square_delta_x = delta_x*delta_x;
            delta_y = sqrt(square_big_radius - square_delta_x); // Pythagorean theorem

            create_new_point_2d(distanc_centers - delta_x, delta_y);
            count_tangent_point_out++;

            create_new_point_2d(ratio * delta_x, -ratio * delta_y);
            count_tangent_point_out++;

            create_new_point_2d(distanc_centers - delta_x, -delta_y);
            count_tangent_point_out++;

            create_new_point_2d(ratio * delta_x, ratio * delta_y);
            count_tangent_point_out++;
        }
    }

//    qDebug() << "point: pre begin remove_new_coordinate_sys_2d";
    remove_new_coordinate_sys_2d(alfa,  x_coordinate_center, y_coordinate_center);
//    qDebug() << "point: end remove_new_coordinate_sys_2d";

    return count_tangent_point_out;

}

int TangentsCalculations::trase_2d(double x_first_point, double y_first_point, double x_second_point, double y_second_point, double distance)
{
//    qDebug() << "point: begin trase_2d. x_first_point = " << x_first_point << "; y_first_point = " << y_first_point << "; x_second_point = " << x_second_point << "; y_second_point = " << y_second_point << "; distance = " << distance << endl;
    double delta_x = (x_first_point - x_second_point);
    double delta_y = (y_first_point - y_second_point);
    double distance_points = sqrt(delta_x*delta_x + delta_y*delta_y);
    double coeff = distance / distance_points;

    double buff =  coeff * abs(delta_x);

    buff = (delta_x*delta_y > 0) ? buff : -buff;

    double new_point_x = x_first_point + buff;
    double new_point_y = y_first_point + coeff * abs(delta_y);
//    qDebug() << "POINT: delta_x = " << delta_x << "; delta_y = " << delta_y << "; distance_points = " << distance_points << "; coeff = " << coeff << "; new_point_x = " << new_point_x << "; new_point_y = " << new_point_y;
    create_new_point_2d(new_point_x, new_point_y);
//    qDebug() << "point: end trase_2d";
    return count_of_point;
}

QMap<QString, QMap<QString, double>> TangentsCalculations::segment_into_line(double x_first_point, double y_first_point, double x_second_point, double y_second_point, double distance)
{
//    qDebug() << "point: begin segment_into_line. x_first_point = " << x_first_point << "; y_first_point = " << y_first_point << "; x_second_point = " << x_second_point << "; y_second_point = " << y_second_point << "; distance = " << distance << endl;
    trase_2d(x_first_point, y_first_point, x_second_point, y_second_point, distance);
    trase_2d(x_second_point, y_second_point, x_first_point, y_first_point, -distance);
//    QMap<QString, QMap<QString, double>> out {{"first-point", pop_element()},{"second-point", pop_element()}};
//    qDebug() << "point: end segment_into_line";
    return {{"first-point", pop_element()},{"second-point", pop_element()}};
}

void TangentsCalculations::change_accuracy(int exponent)
{
    accuracy = 1;
    for (int i = 0; i < exponent; i++)
        accuracy /= 10;
}
//-----------
void TangentsCalculations::test_debug()
{
//    qDebug() << "point: begin test";
    double** matrix = new double*[3];

    points_view_debug();

    for (int i = 0; i < 3; i++)
    {
        matrix[i] = new double[3];
        matrix[i][0] = 0;
        matrix[i][1] = 0;
        matrix[i][2] = 0;
    }

    matrix[0][0] = 1;
    matrix[1][1] = 1;
    matrix[2][2] = 1;

    custom_transform_2d(matrix);

    points_view_debug();


    for (int i = 0; i < 3; i++)
    {
        delete[] matrix[i];
    }
    delete[] matrix;

//    qDebug() << "point: begin test";
}

void TangentsCalculations::points_view_debug()
{
    for (int i = 0; i <= count_of_point; i++)
    {

//        qDebug() << point_matrix[2][i] << " " << point_matrix[1][i] << " " <<point_matrix[0][i] << endl;
    }
}
//----------
void TangentsCalculations::custom_transform_2d(double **transform_matrix)
{
    custom_transform_2d(transform_matrix, 0, count_of_point);
}

int TangentsCalculations::create_new_point_2d(double x, double y)
{
    if (count_of_point < max_count_point)
    {
//        qDebug() << "create_new_point_2d" << "x = " << x << "; y = " << y << "; count_of_point = " << count_of_point + 1;

        point_matrix[0][++count_of_point] = 1;
        point_matrix[1][count_of_point] = y;
        point_matrix[2][count_of_point] = x;
    }
    else
    {
//        qDebug() << "count_of_point >= max_count_point!!!";
    }
    return count_of_point;
}

int TangentsCalculations::create_new_circle_2d(double x, double y, double radius)
{
    return create_new_circle_2d(create_new_point_2d(x, y), radius);
}

int TangentsCalculations::create_new_circle_2d(int number_center, double radius)
{
    circle_array[++count_of_circle].number_center_point = number_center;
    circle_array[count_of_circle].radius = radius;
    return count_of_circle;
}

void TangentsCalculations::delete_last_point()
{
    count_of_point--;
}

QMap<QString, double> TangentsCalculations::pop_element()
{
    count_of_point--;
//    qDebug() << "pop_element : point_matrix[2][count_of_point + 1] = " << point_matrix[2][count_of_point + 1] << "; point_matrix[1][count_of_point + 1] = " << point_matrix[1][count_of_point + 1];
    return {
        {"x", point_matrix[2][count_of_point + 1]},
        {"y", point_matrix[1][count_of_point + 1]},
  };
}
