#include "bspline.h"
#include <cmath>

BSpline::BSpline(int degree, QVector<QVector3D>& points, QVector<float>& knots) :
    points{points}, knots{knots}, degree{degree}
{}

QVector3D BSpline::eval(float point) const
{
    QVector3D result{};
    int i = 0;
    for (QVector3D controlPoint : points) {
        float basisFunctionResult = evalBasisFunction(point, i, degree);
        result += controlPoint * float(basisFunctionResult);
        i++;
    }

    return result;
}

bool BSpline::fequals(float a, float b)
{
    return a - b < 0.00001F;
}


float BSpline::evalBasisFunction(float knot, int i, int deg) const
{
    if (deg == 0) {
        return evalBasisFunctionFirstDegree(knot, i);
    } else {
        // Preventing denominator going to zero and therefore addend to infinity by replacing addend with zero
        float oneDevideByFirstDenominator =
                !fequals(knots[i+deg], knots[i])
                ? 1.0F / (knots[i+deg] - knots[i]) : 0;
        float oneDevideBySecondDenominator =
                !fequals(knots[i+deg+1], knots[i+1])
                ? 1.0F / (knots[i+deg+1] - knots[i+1]) : 0;
        // Recursive Cox De Boor formulae
        return evalBasisFunction(knot, i, deg-1) * (knot - knots[i]) * oneDevideByFirstDenominator +
               evalBasisFunction(knot, i+1, deg-1) * (knots[i+deg+1] - knot) * oneDevideBySecondDenominator;
    }
}


float BSpline::evalBasisFunctionFirstDegree(float t, int i) const
{
    if (knots[i] <= t && t < knots[i+1]) {
        return 1;
    } else {
        return 0;
    }
}
