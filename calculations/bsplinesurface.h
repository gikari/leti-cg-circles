#ifndef BSPLINESURFACE_H
#define BSPLINESURFACE_H

#include <QVector>
#include <QVector3D>
#include <QMatrix4x4>

class BSplineSurface
{
public:
    BSplineSurface();
    BSplineSurface(QVector<QVector<QVector3D>> &polyhedron, int degreeU, int degreeW);

    QVector3D eval(float valueU, float valueW);

    static void generatePolyhedron(QVector<QVector<QVector3D> > &polyhedron, int width, int height);

    void rotateAroundOX(float angle);
    void rotateAroundOY(float angle);
    void rotateAroundOZ(float angle);

    void updatePointX(float value, QPoint pointIndexes);
    void updatePointY(float value, QPoint pointIndexes);
    void updatePointZ(float value, QPoint pointIndexes);

    void setDegreeU(int degree);
    void setDegreeW(int degree);

    const QVector<QVector<QVector3D>>& getPolyheron() const;
    QVector3D& getPolyhedronPointAt(int row, int column);

public slots:

private:
    void transformWithMatrix(const QMatrix4x4 &matrix);
    void resizeKnotVector(QVector<float> &knotVector, int controlPointsSize, int splineDegree);
    QVector<QVector3D> columnVectorOfMatrix(QVector<QVector<QVector3D>> matrix, int column);

    QVector<QVector<QVector3D>> polyhedron;
    QVector<float> knotsU;
    QVector<float> knotsW;
    int degreeU;
    int degreeW;
};

#endif // BSPLINESURFACE_H
